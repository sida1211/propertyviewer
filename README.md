## Notes

* This is a repo for Property Viewer, an iOS app grabs property info from Internet and display them as a list
* This repo is created by Sida Li (sida1211@gmail.com)
* The app will grab property info at launch
* Pull the list can refresh the list
* An alert will show up if no Internet connection and / or other connection issues stop the app from fetching property info
* Property Viewer is an Universal iOS app which supports various device orientations
* Property Viewer has been tested on an iPhone8 (real device) and iPad Air (real device)

---

## Install

1. Clone the repo
2. Open PropertyView workspace
3. Go to Project Settings - General and tick 'Automatically managed signing'
4. Fill an unique Bundle Identifier
5. In CLI, navigate to the project folder and run 'pod install'
6. In Xcode run the project on a simulator or a real device
7. If you need any help, please contact me - sida1211@gmail.com

---

## Libraries & Tools

* Coocapod is used to manage dependencies
* Alamofire is used to build network layer of the app
* RxSwift, RxCocoa and RxDatasource are used to bind data with UI
* SDWebImage is used to download images asynchronously
* SwiftyJSON is used to access raw JSON

---

## Screenshots

![Screenshot_1](/screenshots/IMG_0047.PNG?raw=true)
![Screenshot_2](/screenshots/IMG_2694.PNG?raw=true)
![Screenshot_3](/screenshots/IMG_2695.PNG?raw=true)
![Screenshot_4](/screenshots/IMG_2696.PNG?raw=true)