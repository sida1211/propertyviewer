//
//  MasterViewController.swift
//  PropertyViewer
//
//  Created by Sida Li on 25/5/18.
//  Copyright © 2018 sidali. All rights reserved.
//

import UIKit
import RxDataSources
import RxSwift
import RxCocoa
import SwiftyJSON

class MasterViewController: UIViewController {

    @IBOutlet private var tableView: UITableView!
    
    var detailViewController: DetailViewController? = nil
    
    let disposeBag = DisposeBag()

    // define RxDataSource for tableView
    let dataSource = RxTableViewSectionedReloadDataSource<SectionModel<String, Property>>(
        configureCell: {(ds, tv, ip, item) -> UITableViewCell in
            if item.isPremium {
                let cell = tv.dequeueReusableCell(withIdentifier: "PremiumPropertyCell", for: ip) as! PremiumPropertyViewCell
                cell.configureWith(property: item)
                return cell
            } else {
                let cell = tv.dequeueReusableCell(withIdentifier: "NormalPropertyCell", for: ip) as! NormalPropertyTableViewCell
                cell.configureWith(property: item)
                return cell
            }
        },
        titleForHeaderInSection: { dataSource, section in
            return dataSource.sectionModels[section].model
        }
    )
    
    // MARK: - RefreshControl
    private let refreshControl = UIRefreshControl()
    @objc private func refreshProperties(_ sender: Any) {
        // Fetch properties
        NetworkManager.sharedManager.getProperties(completion:{ result in
            if result {
                self.refreshControl.endRefreshing()
            } else {
                self.showConnectionErrorAlert()
            }
        })
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // configure Refresh Control
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshProperties(_:)), for: .valueChanged)
        
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
        
        // assign tableView datasource, bind it to section model
        DataManager.sharedManager.propertySections.asObservable().bind(to:tableView.rx.items(dataSource: dataSource)).disposed(by:disposeBag)
        // assign tableView delegate
        tableView.rx.setDelegate(self).disposed(by: disposeBag)
        
        // load properties automatically at the launch of the app
        refreshControl.beginRefreshing()
        NetworkManager.sharedManager.getProperties(completion:{ result in
            if result {
                self.refreshControl.endRefreshing()
            } else {
                self.showConnectionErrorAlert()
            }
        })
    }
    
    // MARK: - Alert Message
    // show connection error message to user
    private func showConnectionErrorAlert() {
        let alert = UIAlertController(title: "Connection Problem", message: "Please check your Internet connection and try again.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            NSLog("The \"OK\" alert occured.")
            self.refreshControl.endRefreshing()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                // Pass selected property to DetailVC
                controller.detailItem = dataSource.sectionModels[indexPath.section].items[indexPath.row]
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }
}

// MARK: UITableViewDelegate
// table view delegate to return cell height for different table view cell layout
extension MasterViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return CGFloat(440)
        } else {
            return CGFloat(180)
        }
    }
}
