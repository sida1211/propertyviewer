//
//  NormalPropertyTableViewCell.swift
//  PropertyViewer
//
//  Created by Sida Li on 26/5/18.
//  Copyright © 2018 sidali. All rights reserved.
//

import UIKit
import SDWebImage

class NormalPropertyTableViewCell: UITableViewCell {
    
    static let Identifier = "NormalPropertyCell"
    
    @IBOutlet private var propertyNameLabel: UILabel!
    @IBOutlet private var priceLabel: UILabel!
    @IBOutlet private var addressLine1Label: UILabel!
    @IBOutlet private var addressLine2Label: UILabel!
    @IBOutlet private var suburbPostcodeLabel: UILabel!
    @IBOutlet private var propertyInfoLabel: UILabel!
    @IBOutlet private var ownerAvatarImage: UIImageView!
    @IBOutlet private var ownerNameLabel: UILabel!
    
    func configureWith(property: Property) {
        // disable cell selection highlight
        self.selectionStyle = UITableViewCellSelectionStyle.none
        
        // formatter for property price
        let numberFormatter = NumberFormatter()
        numberFormatter.groupingSeparator = ","
        numberFormatter.groupingSize = 3
        numberFormatter.usesGroupingSeparator = true
        numberFormatter.maximumFractionDigits = 0
        
        propertyNameLabel.text = property.title
        priceLabel.text = "Price - $" + numberFormatter.string(from: property.price as NSNumber)!
        addressLine1Label.text = property.address1
        addressLine2Label.text = property.address2
        suburbPostcodeLabel.text = property.suburb + ", " + property.postcode
        propertyInfoLabel.text = "🛏️" + String(property.bedrooms) + "      🚿" + String(property.bathrooms) + "        🚗" + String(property.carspots)
        
        // set owner image to have a circular frame
        let radius = ownerAvatarImage.frame.width / 2
        ownerAvatarImage.layer.cornerRadius = radius
        ownerAvatarImage.layer.masksToBounds = true
        // add activity indicator for async image loading
        ownerAvatarImage.sd_setShowActivityIndicatorView(true)
        ownerAvatarImage.sd_setIndicatorStyle(.gray)
        ownerAvatarImage.sd_setImage(with: property.avaterUrl, placeholderImage: UIImage(named: "placeholder"))
        
        ownerNameLabel.text = property.ownerFirstName + " " + property.ownerLastName
    }
}
