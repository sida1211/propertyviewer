//
//  PropertyViewCell.swift
//  PropertyViewer
//
//  Created by Sida Li on 25/5/18.
//  Copyright © 2018 sidali. All rights reserved.
//

import UIKit
import SDWebImage

class PremiumPropertyViewCell: UITableViewCell {
    
    static let Identifier = "PremiumPropertyCell"

    @IBOutlet private var propertyNameLabel: UILabel!
    @IBOutlet private var priceLabel: UILabel!
    @IBOutlet private var addressLine1Label: UILabel!
    @IBOutlet private var addressLine2Label: UILabel!
    @IBOutlet private var suburbAndPostcodeLabel: UILabel!
    @IBOutlet private var propertyInfoLabel: UILabel!
    @IBOutlet private var propertyImage: UIImageView!
    @IBOutlet private var ownerAvatarImage: UIImageView!
    @IBOutlet private var ownerNameLabel: UILabel!
        
    func configureWith(property: Property) {
        // disable cell selection highlight
        self.selectionStyle = UITableViewCellSelectionStyle.none
        
        // formatter for property price
        let numberFormatter = NumberFormatter()
        numberFormatter.groupingSeparator = ","
        numberFormatter.groupingSize = 3
        numberFormatter.usesGroupingSeparator = true
        numberFormatter.maximumFractionDigits = 0
        
        propertyNameLabel.text = "  " + property.title
        priceLabel.text = "Price - $" + numberFormatter.string(from: property.price as NSNumber)!
        addressLine1Label.text = property.address1
        addressLine2Label.text = property.address2
        suburbAndPostcodeLabel.text = property.suburb + ", " + property.postcode
        propertyInfoLabel.text = "🛏️" + String(property.bedrooms) + "      🚿" + String(property.bathrooms) + "        🚗" + String(property.carspots)
        ownerNameLabel.text = property.ownerFirstName + " " + property.ownerLastName
        
        // add activity indicator for async image loading
        propertyImage.sd_setShowActivityIndicatorView(true)
        propertyImage.sd_setIndicatorStyle(.gray)
        propertyImage.sd_setImage(with: property.propertyPictureUrl, placeholderImage: UIImage(named: "placeholder"))
        
        // set owner image to have a circular frame
        let radius = ownerAvatarImage.frame.width / 2
        ownerAvatarImage.layer.cornerRadius = radius
        ownerAvatarImage.layer.masksToBounds = true
        ownerAvatarImage.sd_setShowActivityIndicatorView(true)
        ownerAvatarImage.sd_setIndicatorStyle(.gray)
        ownerAvatarImage.sd_setImage(with: property.avaterUrl, placeholderImage: UIImage(named: "placeholder"))
    }
}
