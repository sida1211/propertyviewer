//
//  Property.swift
//  PropertyViewer
//
//  Created by Sida Li on 25/5/18.
//  Copyright © 2018 sidali. All rights reserved.
//

import Foundation

struct Property: Equatable {
    let title: String
    let bedrooms: Int
    let bathrooms: Int
    let carspots: Int
    let description: String
    let price: Float
    let ownerFirstName: String
    let ownerLastName: String
    let avaterUrl: URL
    let address1: String
    let address2: String
    let suburb: String
    let postcode: String
    let isPremium: Bool
    let propertyPictureUrl: URL
    let id: Int
}
