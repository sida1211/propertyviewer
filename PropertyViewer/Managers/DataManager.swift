//
//  DataManager.swift
//  PropertyViewer
//
//  Created by Sida Li on 25/5/18.
//  Copyright © 2018 sidali. All rights reserved.
//

import Foundation
import RxSwift
import RxDataSources
import SwiftyJSON

class DataManager: NSObject {
    static let sharedManager = DataManager()
    
    private override init() {
        super.init()
//        print("DataManager initialized")
        
        // bind properties to RxSectionModel propertySections
        properties.asObservable().map
            { (properties: [Property]) -> [SectionModel<String, Property>] in
                let normalProperties = properties.filter({!$0.isPremium})
                let premiumProperties = properties.filter({$0.isPremium})
                
                return [SectionModel(model: "Premium", items: premiumProperties), SectionModel(model: "Others", items: normalProperties)]
            }.bind(to: propertySections).disposed(by: disposeBag)
    }
    
    let disposeBag = DisposeBag()
    var properties: Variable<[Property]> = Variable([])
    let propertySections = PublishSubject<[SectionModel<String, Property>]>()
    
    // interpret SwiftyJSON objects to Property Struct and add to properties
    func addProperties(from JSON:JSON) {
        properties.value.removeAll()

        for (_, subJSON):(String, JSON) in JSON {
            
            properties.value.append(Property(title: subJSON["title"].string!,
                                             bedrooms: subJSON["bedrooms"].int!,
                                             bathrooms: subJSON["bathrooms"].int!,
                                             carspots: subJSON["carspots"].int!,
                                             description: subJSON["description"].string!,
                                             price: subJSON["price"].float!,
                                             ownerFirstName: subJSON["owner"]["first_name"].string!,
                                             ownerLastName: subJSON["owner"]["last_name"].string!,
                                             avaterUrl: URL(string:subJSON["owner"]["avatar"]["medium"]["url"].string!)!,
                                             address1: subJSON["location"]["address_1"].string!,
                                             address2: subJSON["location"]["address_2"].string!,
                                             suburb: subJSON["location"]["suburb"].string!,
                                             postcode: subJSON["location"]["postcode"].string!,
                                             isPremium: subJSON["is_premium"].bool!,
                                             propertyPictureUrl: URL(string:subJSON["photo"]["image"]["url"].string!)!,
                                             id: subJSON["id"].int!
                )
            )
        }
    }
}
