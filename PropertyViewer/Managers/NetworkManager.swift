//
//  NetWorkManager.swift
//  PropertyViewer
//
//  Created by Sida Li on 25/5/18.
//  Copyright © 2018 sidali. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class NetworkManager: NSObject {
    static let sharedManager = NetworkManager()
    
    public func getProperties(completion: @escaping (Bool)->()) {
        Alamofire.request(APPURL.AllProperties, method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                // extract property list from raw respond, and pass it to DataManager as SwiftyJSON object
                let json = JSON(value)
                let  data = json["data"]
                
                DataManager.sharedManager.addProperties(from: data)
                
                completion(true)
            case .failure(let error):
                print(error)
                completion(false)
            }
        }
    }
}
