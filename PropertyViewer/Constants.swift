//
//  Constants.swift
//  PropertyViewer
//
//  Created by Sida Li on 27/5/18.
//  Copyright © 2018 sidali. All rights reserved.
//

import Foundation

struct APPURL {
    
    private struct Domains {
        static let Dev = "http://demo0065087.mockable.io"
    }
    
    private  struct Routes {
        static let Api = "/test"
    }
    
    private  static let Domain = Domains.Dev
    private  static let Route = Routes.Api
    private  static let BaseURL = Domain + Route
    
    static var AllProperties: String {
        return BaseURL  + "/properties"
    }
}
