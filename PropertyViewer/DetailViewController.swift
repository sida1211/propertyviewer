//
//  DetailViewController.swift
//  PropertyViewer
//
//  Created by Sida Li on 25/5/18.
//  Copyright © 2018 sidali. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet private var idValueLabel: UILabel!
    @IBOutlet private var titleValueLabel: UILabel!
    

    func configureView() {
        // Update the user interface for the detail item.
        if let detail = detailItem {
            if let label = titleValueLabel {
                label.text = detail.title
            }
            if let label = idValueLabel {
                label.text = String(detail.id)
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureView()
    }

    var detailItem: Property? {
        didSet {
            // Update the view.
            configureView()
        }
    }


}

